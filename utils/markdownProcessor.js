import { unified } from 'unified';
import parse from 'remark-parse';
import rehype from 'remark-rehype';
import toReact from 'rehype-react';
import {createElement} from 'react';

export default unified()
    .use(parse)
    .use(rehype)
    .use(toReact, {createElement});
