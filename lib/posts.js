import {readdirSync, readFileSync} from 'fs';
import {join} from 'path';
import matter from 'gray-matter';

const directory = join(process.cwd(), 'posts');

export function getPosts() {

    return readdirSync(directory)
        .map(filename => {
            const id = filename.replace(/\.md$/, '');
            const fileContent = readFileSync(join(directory, filename), 'utf8');
            const {data} = matter(fileContent);

            return {
                id,
                ...data,
            };
        })
        .sort((a, b) => a.date - b.date);
}

export function getPost(id) {
    const fileContent = readFileSync(join(directory, `${id}.md`));
    const {data, content} = matter(fileContent);

    return {
        id,
        content,
        ...data,
    };
}

export function getPostIds() {
    return readdirSync(directory).map(filename => filename.replace(/\.md$/, ''));
}
