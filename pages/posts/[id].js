import Head from 'next/head';
import Image from 'next/image';
import Layout from '../../components/Layout';
import { getPost, getPostIds } from '../../lib/posts';
import processor from '../../utils/markdownProcessor';


export default function Post({id, title, date, content}) {

    return (
        <Layout>
            <Head>
                <title>{title}</title>
            </Head>
            <h1>{title}</h1>
            <time>{date}</time>

            <div>
            {
                processor
                    .processSync(content)
                    .result
            }
            </div>

            <Image src="/images/profile.jpg"
                width={800}
                height={400}
                alt="Webmaster's profile picture" />
        </Layout>
    );
}

export async function getStaticPaths() {
    return {
        paths: getPostIds().map(id => ({params: {id}})),
        fallback: false,
    };
}

export async function getStaticProps({params}) {
    return {
        props: {
            ...getPost(params.id),
        },
    };
}
