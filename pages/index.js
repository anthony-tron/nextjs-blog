import Head from 'next/head';
import Link from 'next/link';
import Layout from '../components/Layout';
import { getPosts } from '../lib/posts';

export default function Home({ posts }) {
  ;
  return (
    <Layout home>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ul>
        {posts.map(({id, date, title}) => (
          <li key={id}>
            <header>
              <Link href={`/posts/${id}`}>
                {title}
              </Link>
            </header>
            <time dateTime={date}>{date}</time>
          </li>
        ))}
      </ul>
    </Layout>
  );
}

export async function getStaticProps() {
  return {
    props: {
      posts: getPosts(),
    }
  }
}