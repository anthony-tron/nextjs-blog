import Image from 'next/image';
import Link from 'next/link';
import {container, header, backToHome} from './Layout.module.css';
import {borderCircle} from '../styles/utils.module.css';

export default function Layout({children, home}) {
    return (
        <section className={container}>
            <header className={header}>
                {home ? (
                    <>
                        <Image
                            priority
                            src="/images/profile.jpg"
                            width={144}
                            height={144}
                            className={borderCircle}
                            alt="Profile picture" />
                    </>
                ) : (
                    <>
                        <Link href="/">
                            <a>nextjs blog</a>
                        </Link>
                        <Image
                            priority
                            src="/images/profile.jpg"
                            width={108}
                            height={108}
                            className={borderCircle}
                            alt="Profile picture" />
                    </>
                )}
            </header>
            <main>
                {children}
            </main>

            {!home && (
                <Link className={backToHome} href="/">&larr; Back to home</Link>
            )}
        </section>
    );
}
